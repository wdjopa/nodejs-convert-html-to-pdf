// app.js
const express = require("express");
const axios = require("axios");
const app = express();
const router = express.Router();
const path = require("path");
const qrCode = require("qrcode");
const puppeteer = require("puppeteer");
const ejs = require("ejs"); // add this line
const fs = require("fs");
const archiver = require("archiver");

app.use(express.static("public"));
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "/views"));

function imageToDataUrl(imgPath) {
  const data = fs.readFileSync(imgPath);
  const ext = path.extname(imgPath);
  const base64 = data.toString("base64");
  return `data:image/${ext.slice(1)};base64,${base64}`;
}
function ucfirst(string = "") {
  return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

router.get("/pdf", async (req, res) => {
  const { group_name = "" } = req.query;
  const request = await axios.get(
    `https://soiree.wilmar.djopa.fr/api/guests?group_name=${group_name}`
  );
  const response = request.data;

  let archive = archiver("zip");
  res.attachment(`tickets_${group_name}.zip`);
  archive.pipe(res);
  let names = [];
  for (let guest of response) {
    try {
      const { id, slug, first_name, last_name, is_couple } = guest;

      const name = `${ucfirst(first_name)} ${ucfirst(last_name)}`;
      console.log("generating ticket for " + name + " -— slug : " + slug);
      let qr1 = await qrCode.toDataURL(
        "https://soiree.wilmar.djopa.fr/guests/" + slug
      );
      let img_1 = imageToDataUrl(
        path.join(__dirname, "public", "images", "1.png")
      );
      let img_2 = imageToDataUrl(
        path.join(__dirname, "public", "images", "2.png")
      );
      let img_3 = imageToDataUrl(
        path.join(__dirname, "public", "images", "3.png")
      );
      let img_5 = imageToDataUrl(
        path.join(__dirname, "public", "images", "5.png")
      );
      let img_6 = imageToDataUrl(
        path.join(__dirname, "public", "images", "6.png")
      );

      // Render the EJS template with the QR codes and images
      let html = await ejs.renderFile(
        path.join(__dirname, "views", "template.ejs"),
        {
          name,
          qr1,
          img_1,
          img_2,
          img_3,
          img_5,
          img_6,
          is_couple: is_couple ? "Billet couple" : "",
        }
      );
      // Use puppeteer to generate the PDF
      const browser = await puppeteer.launch();
      const page = await browser.newPage();
      await page.setContent(html);
      await page.emulateMediaType("screen");
      await page.pdf({
        path: `ticket-${slug}.pdf`,
        format: "A4",
        printBackground: true,
      });
      await browser.close();
      archive.append(
        fs.createReadStream(path.join(__dirname, `ticket-${slug}.pdf`)),
        { name: `ticket-${slug}.pdf` }
      );
      names.push(slug);
    } catch (e) {
      console.error(e);
    }

    // // Send the PDF file
    // res.sendFile(path.join(__dirname, `ticket-${name}.pdf`));
  }
  archive.finalize();
  for (let name of names) {
    try {
      await fs.unlinkSync(
        path.join(__dirname, "tickets", `ticket-${name}.pdf`)
      );
    } catch (error) {
      console.error(error);
    }
  }
});

router.get("/single", async (req, res) => {
  const { id, slug, name = "", is_couple = "true" } = req.query;

  let qr1 = await qrCode.toDataURL(
    "https://soiree.wilmar.djopa.fr/guest/" + slug
  );
  console.log({ qr1 });
  let img_1 = imageToDataUrl(path.join(__dirname, "public", "images", "1.png"));
  let img_2 = imageToDataUrl(path.join(__dirname, "public", "images", "2.png"));
  let img_3 = imageToDataUrl(path.join(__dirname, "public", "images", "3.png"));
  let img_5 = imageToDataUrl(path.join(__dirname, "public", "images", "5.png"));
  let img_6 = imageToDataUrl(path.join(__dirname, "public", "images", "6.png"));

  // Render the EJS template with the QR codes and images
  let html = await ejs.renderFile(
    path.join(__dirname, "views", "template.ejs"),
    {
      name,
      is_couple: is_couple === "true" ? "Billet couple" : "",
      qr1,
      img_1,
      img_2,
      img_3,
      img_5,
      img_6,
    }
  );

  // Use puppeteer to generate the PDF
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  // await page.setContent(html);
  await page.setContent(html, { waitUntil: "networkidle0" });

  await page.emulateMediaType("screen");
  await page.pdf({
    path: `ticket-${name}.pdf`,
    format: "A4",
    printBackground: true,
  });
  await browser.close();

  res.sendFile(path.join(__dirname, `ticket-${name}.pdf`));
});

app.use("/", router);
app.listen(3300, () => console.log("Listening on port 3300"));
