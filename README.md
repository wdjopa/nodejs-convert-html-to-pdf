## Contexte

Pour mon mariage, j'ai souhaité généré les billets de mes invités numériquement.

À partir de la liste de mes invités disponibles via une API, je génère le billet à partir des images de mon billet que je convertis ensuite en PDF, en ajoutant un nom et un QRCode sur chaque billet. Ces billets sont ensuite enregistrés dans un archive avant d'être accessible.
